﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SparkUI.Xamarin.ProgressBar.Helper
{
    internal class SegmentIndicator
    {
        internal int StartPoint { get; set; }

        internal bool IsFilled { get; set; }
    }
}
