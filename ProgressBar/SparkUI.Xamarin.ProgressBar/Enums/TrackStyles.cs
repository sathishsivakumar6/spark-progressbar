﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SparkUI.Xamarin.ProgressBar
{
    public enum TrackStyles
    {
        StrokeOnly,
        FillOnly,
        StrokeAndFill
    }
}
