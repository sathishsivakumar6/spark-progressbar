﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SparkUI.Xamarin.ProgressBar.Constants
{
    internal static class RadialProgressBarConstants
    {
        internal const int TotalDegree = 360;

        internal const int MaxIndeterminateSpeed = 10;

        internal const int SegmentGap = 2;
    }
}
