﻿using SkiaSharp.Views.Forms;
using SparkUI.Xamarin.ProgressBar.Helper;
using System;
using System.Collections.Generic;
using Xamarin.Forms;
using Consts = SparkUI.Xamarin.ProgressBar.Constants.RadialProgressBarConstants;

namespace SparkUI.Xamarin.ProgressBar.ProgressBarBase
{
	public abstract class SparkProgressBar : SKCanvasView, IDisposable
    {
        public static readonly BindableProperty ProgressProperty =
            BindableProperty.Create(nameof(Progress), typeof(float), typeof(SparkProgressBar), 0.0f, BindingMode.Default, propertyChanged: OnProgressChanged);

        public static readonly BindableProperty ProgressBarTrackColorProperty =
            BindableProperty.Create(nameof(ProgressBarTrackColor), typeof(Color), typeof(SparkProgressBar), Color.LightGray, BindingMode.Default);

        public static readonly BindableProperty ProgressBarIndicatorColorProperty =
            BindableProperty.Create(nameof(ProgressBarIndicatorColor), typeof(Color), typeof(SparkProgressBar), Color.Blue, BindingMode.Default);

        public static readonly BindableProperty ProgressBarTrackStrokeWidthProperty =
            BindableProperty.Create(nameof(ProgressBarTrackStrokeWidth), typeof(float), typeof(SparkProgressBar), 10.0f, BindingMode.Default);

        public static readonly BindableProperty ProgressBarIndicatorStrokeWidthProperty =
            BindableProperty.Create(nameof(ProgressBarIndicatorStrokeWidth), typeof(float), typeof(SparkProgressBar), 10.0f, BindingMode.Default);

		

		/// <summary>
		/// Gets or sets the current progress
		/// </summary>
		/// <value>The progress.</value>
		/// <remarks>The progress value should lies between 0 and 100.</remarks>
		public float Progress
        {
            get { return (float)GetValue(ProgressProperty); }
            set
            {
                SetValue(ProgressProperty, value);
            }
        }

		/// <summary>
		/// Gets or sets the inner stroke width of the progress bar.
		/// </summary>
		/// <value>The stroke width.</value>
		public float ProgressBarTrackStrokeWidth
		{
			get { return (float)GetValue(ProgressBarTrackStrokeWidthProperty); }
			set { SetValue(ProgressBarTrackStrokeWidthProperty, value);	}
		}

		/// <summary>
		/// Gets or sets the outer stroke width of the progress bar.
		/// </summary>
		/// <value>The stroke width.</value>
		public float ProgressBarIndicatorStrokeWidth
		{
			get { return (float)GetValue(ProgressBarIndicatorStrokeWidthProperty); }
			set { SetValue(ProgressBarIndicatorStrokeWidthProperty, value);	}
		}

		/// <summary>
		/// Gets or sets the ProgressBackgroundColorProperty
		/// </summary>
		/// <value>The color of the ProgressBackgroundColorProperty.</value>
		public Color ProgressBarTrackColor
		{
			get { return (Color)GetValue(ProgressBarTrackColorProperty); }
			set { SetValue(ProgressBarTrackColorProperty, value); }
		}


		/// <summary>
		/// Gets or sets the radial progress bar color.
		/// </summary>
		/// <value>The color of the radial progress bar.</value>
		public Color ProgressBarIndicatorColor
		{
			get { return (Color)GetValue(ProgressBarIndicatorColorProperty); }
			set { SetValue(ProgressBarIndicatorColorProperty, value); }
		}

		#region InternalFields
		internal IList<float> SegmentAngles
		{
			get; set;
		}

		internal float ArcAngleOfSegment
		{
			get; set;
		}

		#endregion

		#region CallBackMethods
		private static void OnProgressChanged(BindableObject bindable, object oldValue, object newValue)
		{
			SparkProgressBar progressBar = (SparkProgressBar)bindable;
			progressBar.InvalidateSurface();
		}

		//private static void OnSegmentsChanged(BindableObject bindable, object oldValue, object newValue)
		//{
		//	SparkProgressBar progressBar = (SparkProgressBar)bindable;
		//	if (progressBar is RadialProgressBar && oldValue != newValue)
		//	{
		//		RadialProgressBar radialProgressBar = (progressBar as RadialProgressBar);
		//		radialProgressBar.Reset();
		//		radialProgressBar.SegmentAngles = new List<float>();
		//		radialProgressBar.ArcAngleOfSegment = (float)Consts.TotalDegree / radialProgressBar.Segments;
		//		radialProgressBar.BackupStartAngle = radialProgressBar.StartAngle;
		//		radialProgressBar.SegmentStartAngle = radialProgressBar.BackupStartAngle;
		//		float startAngle = radialProgressBar.BackupStartAngle;
		//		for (int i = 0; i < radialProgressBar.Segments; i++)
		//		{
		//			radialProgressBar.SegmentAngles.Add(startAngle);
		//			startAngle += radialProgressBar.ArcAngleOfSegment;
		//			if (startAngle >= Consts.TotalDegree)
		//			{
		//				startAngle = (startAngle - Consts.TotalDegree);
		//			}
		//		}
		//	}

		//}

		public void Dispose()
		{
			this.Dispose();
			this.Reset();
		}

		internal void Reset()
		{
			SegmentAngles?.Clear();
		}
		#endregion
	}
}
