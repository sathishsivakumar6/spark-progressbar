﻿using SkiaSharp;
using SkiaSharp.Views.Forms;
using SparkUI.Xamarin.ProgressBar.ProgressBarBase;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;
using Consts = SparkUI.Xamarin.ProgressBar.Constants.RadialProgressBarConstants;
using System.Linq;
using System;

namespace SparkUI.Xamarin.ProgressBar
{
	public class RadialProgressBar : SparkProgressBar, IDisposable
	{
        #region BindableProperty
        public static readonly BindableProperty IndeterminateProperty =
			BindableProperty.Create(nameof(Indeterminate), typeof(bool), typeof(RadialProgressBar), false, BindingMode.Default, propertyChanged: OnIndeterminateRunning);
		
		public static readonly BindableProperty RadiusProperty =
			BindableProperty.Create(nameof(Radius), typeof(float), typeof(RadialProgressBar), 100.0f, BindingMode.Default);

		public static readonly BindableProperty IndeterminateSpeedProperty =
			BindableProperty.Create(nameof(IndeterminateSpeed), typeof(float), typeof(RadialProgressBar), 5.0f, BindingMode.Default);

		public static readonly BindableProperty StartAngleProperty =
			BindableProperty.Create(nameof(StartAngle), typeof(float), typeof(RadialProgressBar), 270.0f, BindingMode.Default, propertyChanged: OnSegmentsProperyChanged);

		public static readonly BindableProperty SweepAngleProperty =
			BindableProperty.Create(nameof(SweepAngle), typeof(float), typeof(RadialProgressBar), 90.0f, BindingMode.Default);

		public static readonly BindableProperty TrackStylesProperty =
			BindableProperty.Create(nameof(TrackStyles), typeof(TrackStyles), typeof(RadialProgressBar), TrackStyles.StrokeOnly, BindingMode.Default);

		public static readonly BindableProperty ShowProgressProperty =
			BindableProperty.Create(nameof(ShowProgress), typeof(bool), typeof(RadialProgressBar), true, BindingMode.Default);

		public static readonly BindableProperty ContentSizeProperty =
			BindableProperty.Create(nameof(ContentSize), typeof(float), typeof(RadialProgressBar), 64.0f, BindingMode.Default);

		public static readonly BindableProperty ContentProperty =
			BindableProperty.Create(nameof(Content), typeof(string), typeof(RadialProgressBar), string.Empty, BindingMode.Default);

		public static readonly BindableProperty ContentColorProperty =
			BindableProperty.Create(nameof(ContentColor), typeof(Color), typeof(RadialProgressBar), Color.Default, BindingMode.Default);
		
		public static readonly BindableProperty SegmentsProperty =
			BindableProperty.Create(nameof(Segments), typeof(int), typeof(RadialProgressBar), 1, BindingMode.Default, propertyChanged: OnSegmentsProperyChanged);

		public int Segments
		{
			get { return (int)GetValue(SegmentsProperty); }
			set { SetValue(SegmentsProperty, value); }
		}
		/// <summary>
		/// Gets or sets if radial progress bar is in indeterminate state.
		/// </summary>
		/// <value>The indeterminate state of radial progress bar.</value>
		public bool Indeterminate
		{
			get { return (bool)GetValue(IndeterminateProperty); }
			set { SetValue(IndeterminateProperty, value); }
		}

		/// <summary>
		/// Gets or sets the radial progress bar track style.
		/// </summary>
		/// <value>The radial progress bar track style.</value>

		public TrackStyles TrackStyles
		{
			get { return (TrackStyles)GetValue(TrackStylesProperty); }
			set { SetValue(TrackStylesProperty, value); }
		}

		/// <summary>
		/// Gets or sets the content size of radial progress bar.
		/// </summary>
		/// <value>The content size of radial progress bar.</value>
		public float ContentSize
		{
			get { return (float)GetValue(ContentSizeProperty); }
			set { SetValue(ContentSizeProperty, value); }
		}

		/// <summary>
		/// Gets or sets the content of radial progress bar.
		/// </summary>
		/// <value>The content of radial progress bar.</value>
		public string Content
		{
			get { return (string)GetValue(ContentProperty); }
			set { SetValue(ContentProperty, value);	}
		}

		/// <summary>
		/// Gets or sets the radius of the circle represents radial progress bar.
		/// </summary>
		/// <value>The radius of the circle represents radial progress bar.</value>
		public float Radius
		{
			get { return (float)GetValue(RadiusProperty); }
			set { SetValue(RadiusProperty, value); }
		}

		/// <summary>
		/// Gets or sets the content color of the radial progress bar.
		/// </summary>
		/// <value>The content color of the radial progress bar.</value>
		public Color ContentColor
		{
			get { return (Color)GetValue(ContentColorProperty); }
			set { SetValue(ContentColorProperty, value); }
		}

		/// <summary>
		/// Gets or sets the indeterminate speed of radial progress bar.
		/// </summary>
		/// <value>The indeterminate speed of radial progress bar should lies between 1 and 10.</value>
		public float IndeterminateSpeed
		{
			get { return (float)GetValue(IndeterminateSpeedProperty); }
			set { SetValue(IndeterminateSpeedProperty, value); }
		}

		/// <summary>
		/// Gets or sets the start angle to draw the radial progress bar.
		/// </summary>
		public float StartAngle
		{
			get { return (float)GetValue(StartAngleProperty); }
			set 
			{ 
				SetValue(StartAngleProperty, value);
			}
		}

		/// <summary>
		/// Gets or sets the angle to be sweeped with respect to the start angle of the radial progress bar.
		/// </summary>
		public float SweepAngle
		{
			get { return (float)GetValue(SweepAngleProperty); }
			set { SetValue(SweepAngleProperty, value); }
		}

		/// <summary>
		/// Gets or sets the progress is being displayed at the center of radial progress bar.
		/// </summary>
		public bool ShowProgress
		{
			get { return (bool)GetValue(ShowProgressProperty); }
			set { SetValue(ShowProgressProperty, value); }
		}

        #endregion

        #region private_fields
        private SKPaint _inderminateProgressPaint;
		private List<float> _completedSegments = new List<float>();

		#endregion

		#region internal_property
		internal float BackupStartAngle { get; set; }
		internal float SegmentStartAngle { get; set; }
		internal SKPaint IndeterminateProgressPaint
		{
			get
			{
				if(_inderminateProgressPaint==null)
				{
					_inderminateProgressPaint = new SKPaint
					{
						Style = SKPaintStyle.Stroke,
						Color = ProgressBarIndicatorColor.ToSKColor(),
						IsAntialias = true,
						IsDither = true,
						StrokeWidth = ProgressBarIndicatorStrokeWidth
					};
				}
				return _inderminateProgressPaint;
			}
		}
        #endregion

        protected override void OnPaintSurface(SKPaintSurfaceEventArgs args)
		{
			base.OnPaintSurface(args);
			SKImageInfo imageInfo = args.Info;
			SKSurface surface = args.Surface;
			SKCanvas canvas = surface.Canvas;
			SKRect rect = new SKRect((imageInfo.Width - Radius) / 2, (imageInfo.Height - Radius) / 2, (imageInfo.Width + Radius) / 2, (imageInfo.Height + Radius) / 2);
			canvas.Clear();
			canvas.Save();
			DrawRadialProgress(canvas, rect);
			canvas.Restore();
		}

		private void DrawRadialProgress(SKCanvas canvas, SKRect rect)
		{
			if (!Indeterminate)
			{
				if (Segments <= 1)
				{
					// Draw progress track.
					DrawProgressTrack(canvas, rect);
					// Draw progress indicator.
					DrawProgressIndicator(canvas, rect);
					// Draw Text that showing progress in radial progress bar
					DrawText(canvas, rect);
				}
				else
				{
					// Draw progress track.
					DrawProgressTrackSegments(canvas, rect);
					// Draw progress indicator.
					DrawProgressIndicatorSegments(canvas, rect);
				}
				
			}
			else
			{
				// Draw progress track.
				DrawProgressTrack(canvas, rect);
				// Draw indeterminate progress.
				DrawIndeterminateProgress(canvas, rect);
			}
		}

		private void DrawProgressIndicatorSegments(SKCanvas canvas, SKRect rect)
		{
			SKPaint paint = new SKPaint
			{
				Style = SKPaintStyle.Stroke,
				Color = ProgressBarIndicatorColor.ToSKColor(),
				IsAntialias = true,
				IsDither = true,
				StrokeWidth = ProgressBarIndicatorStrokeWidth
			};
			float sweepAngle = ((float)Consts.TotalDegree * (Progress / 100));

			float currentSegmentSweep = 0;
			using (SKPath path = new SKPath())
			{
				if(_completedSegments.Count>0)
				{
					foreach(float completedSegment in _completedSegments)
					{
						path.AddArc(rect, completedSegment, ArcAngleOfSegment-Consts.SegmentGap);
						currentSegmentSweep += (float)ArcAngleOfSegment;
					}
				}
				float thresholdSweep = (sweepAngle - currentSegmentSweep);
				float segmentSweep = thresholdSweep > ArcAngleOfSegment ? ArcAngleOfSegment : thresholdSweep;
				if (segmentSweep == 0)
					return;
				path.AddArc(rect, SegmentStartAngle, segmentSweep - Consts.SegmentGap);
				canvas.DrawPath(path, paint);
				UpdateSweepSegment(segmentSweep);
			}
		}

		/// <summary>
		/// Draw the indeterminate radial progress.
		/// </summary>
		/// <param name="canvas">The drawing canvas.</param>
		/// <param name="rect">The rectangle portion of the drawing surface.</param>
		private void DrawIndeterminateProgress(SKCanvas canvas, SKRect rect)
		{
			if (IndeterminateSpeed > Consts.MaxIndeterminateSpeed)
			{
				IndeterminateSpeed = Consts.MaxIndeterminateSpeed;
			}

			StartAngle += IndeterminateSpeed;
			if (StartAngle >= Consts.TotalDegree)
			{
				StartAngle = (StartAngle - Consts.TotalDegree);
			}

			using (SKPath path = new SKPath())
			{
				path.AddArc(rect, StartAngle, SweepAngle);
				canvas.DrawPath(path, IndeterminateProgressPaint);
			}
		}

		/// <summary>
		/// Draw the progress indicator.
		/// </summary>
		/// <param name="canvas">The drawing canvas.</param>
		/// <param name="rect">The rectangle portion of the drawing surface.</param>
		private void DrawProgressIndicator(SKCanvas canvas, SKRect rect)
		{
			SKPaint paint = new SKPaint
			{
				Style = SKPaintStyle.Stroke,
				Color = ProgressBarIndicatorColor.ToSKColor(),
				IsAntialias = true,
				IsDither = true,
				StrokeWidth = ProgressBarIndicatorStrokeWidth
			};

			float sweepAngle = Consts.TotalDegree * (Progress / 100);
			using (SKPath path = new SKPath())
			{
				path.AddArc(rect, StartAngle, sweepAngle);
				canvas.DrawPath(path, paint);
			}

		}

		/// <summary>
		/// Draw the track.
		/// </summary>
		/// <param name="canvas">The drawing canvas.</param>
		/// <param name="rect">The rectangle portion of the drawing surface.</param>
		private void DrawProgressTrack(SKCanvas canvas, SKRect rect)
		{
			SKPaint paint = new SKPaint
			{
				Color = ProgressBarTrackColor.ToSKColor(),
				IsAntialias = true,
				IsDither = true,
				StrokeWidth = ProgressBarTrackStrokeWidth
			};
			if (this.TrackStyles == TrackStyles.StrokeOnly)
			{
				paint.Style = SKPaintStyle.Stroke;
				paint.IsStroke = true;
			}
			else if (this.TrackStyles == TrackStyles.FillOnly)
			{
				paint.Style = SKPaintStyle.Fill;
				paint.IsStroke = false;
			}
			else if (this.TrackStyles == TrackStyles.StrokeAndFill)
			{
				paint.Style = SKPaintStyle.StrokeAndFill;
			}
			canvas.DrawOval(rect, paint);
		}

		/// <summary>
		/// Draw the track.
		/// </summary>
		/// <param name="canvas">The drawing canvas.</param>
		/// <param name="rect">The rectangle portion of the drawing surface.</param>
		private void DrawProgressTrackSegments(SKCanvas canvas, SKRect rect)
		{
			SKPaint paint = new SKPaint
			{
				Color = ProgressBarTrackColor.ToSKColor(),
				IsAntialias = true,
				IsDither = true,
				StrokeWidth = ProgressBarTrackStrokeWidth,
				Style = SKPaintStyle.Stroke,
				IsStroke = true
			};


			using (SKPath path = new SKPath())
			{
				foreach (float segment in SegmentAngles)
				{
					path.AddArc(rect, segment, (float)(ArcAngleOfSegment-Consts.SegmentGap));
				}
				canvas.DrawPath(path, paint);
			}
		}

		/// <summary>
		/// The text showing at the center of the radial progress.
		/// </summary>
		/// <param name="canvas">The drawing canvas.</param>
		/// <param name="rect">The rectangle portion of the drawing surface.</param>
		private void DrawText(SKCanvas canvas, SKRect rect)
		{
			if (ShowProgress)
			{
				SKPaint paint = new SKPaint
				{
					IsAntialias = true,
					IsDither = true,
					TextAlign = SKTextAlign.Center,
					TextSize = ContentSize,
				};

				paint.Color = ContentColor == Color.Default ? ProgressBarIndicatorColor.ToSKColor() :
															  ContentColor.ToSKColor();
				string content = string.IsNullOrWhiteSpace(Content) ? Progress.ToString() + "%" :
															  Content;
				canvas.DrawText(content, rect.MidX, rect.MidY, paint);
			}
		}

		
		/// <summary>
		/// Called when switching to inderminate progress bar.
		/// </summary>
		/// <param name="bindable">The bindable object <see cref="RadialProgressBar"/></param>
		/// <param name="oldValue">The old value.</param>
		/// <param name="newValue">The new value.</param>
		private static async void OnIndeterminateRunning(BindableObject bindable, object oldValue, object newValue)
		{
			RadialProgressBar radialProgressBar = (RadialProgressBar)bindable;

			while (radialProgressBar.Indeterminate)
			{
				await Task.Run(() =>
				{
					radialProgressBar.InvalidateSurface();
				});
			}
		}

		private static void OnSegmentsProperyChanged(BindableObject bindable, object oldValue, object newValue)
		{
			RadialProgressBar radialProgressBar = (RadialProgressBar)bindable;
			if (oldValue != newValue && radialProgressBar.Segments > 1)
			{
				radialProgressBar.UpdateSegmentProgressInfo(radialProgressBar);
			}
		}

		private void UpdateSegmentProgressInfo(RadialProgressBar radialProgressBar)
		{
			if (radialProgressBar.Segments > 1)
			{
				radialProgressBar.ResetProgress();
				radialProgressBar.SegmentAngles = new List<float>();
				radialProgressBar.ArcAngleOfSegment = (float)Consts.TotalDegree / radialProgressBar.Segments;
				radialProgressBar.BackupStartAngle = radialProgressBar.StartAngle;
				radialProgressBar.SegmentStartAngle = radialProgressBar.BackupStartAngle;
				float startAngle = radialProgressBar.BackupStartAngle;
				for (int i = 0; i < radialProgressBar.Segments; i++)
				{
					radialProgressBar.SegmentAngles.Add(startAngle);
					startAngle += radialProgressBar.ArcAngleOfSegment;
					if (startAngle >= Consts.TotalDegree)
					{
						startAngle = (startAngle - Consts.TotalDegree);
					}
				}
			}
		}

		private void UpdateSweepSegment(float currentSweepAngle)
		{
			float sweepAngleCompletedSegment = SegmentStartAngle + currentSweepAngle;
			if (sweepAngleCompletedSegment >= Consts.TotalDegree)
			{
				sweepAngleCompletedSegment -= Consts.TotalDegree;
			}
			if (SegmentAngles.Contains(sweepAngleCompletedSegment) && currentSweepAngle != 0)
			{
				int i = SegmentAngles.IndexOf(sweepAngleCompletedSegment);
				_completedSegments.Add(SegmentStartAngle);
				SegmentStartAngle = SegmentAngles[i];
				if (SegmentStartAngle >= Consts.TotalDegree)
				{
					SegmentStartAngle -= Consts.TotalDegree;
				}
			}
		}

		public void ResetProgress()
		{
			if(Segments>1)
			{
				_completedSegments.Clear();
				SegmentStartAngle = BackupStartAngle;
			}
		}
	}
}

