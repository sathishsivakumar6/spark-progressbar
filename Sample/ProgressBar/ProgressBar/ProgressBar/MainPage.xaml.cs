﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ProgressBar
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            //ProgressBar.Dispose();
        }
        private async void CreateProgress()
        {
            for(int i=1;i<=75; i++)
            {
                UpdateProgress((float)i);
                await Task.Delay(10);
            }
        }
        void UpdateProgress(float obj)
        {
            Device.BeginInvokeOnMainThread(() => ProgressBar.Progress = obj);
        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            ProgressBar.ResetProgress();
            CreateProgress();
        }
    }
}
